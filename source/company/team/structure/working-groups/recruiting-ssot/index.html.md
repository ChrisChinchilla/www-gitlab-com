---
layout: markdown_page
title: "Recruiting SSOT"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value          |
|-----------------|----------------|
| Date Created    | December 30, 2019   |
| End Date        | TBD |
| Slack           | [#wg_recruiting-ssot](https://gitlab.slack.com/archives/CS4T040MS) (only accessible from within the company) | 
| Google Doc      | [Recruiting SSOT WG Agenda](https://docs.google.com/document/d/14kg9B7DqPoqJEja_hDAwdVEKMi4DyounDaRwjo2whKo/edit) (only accessible from within the company) |

## Business Goal

To create a Recruiting SSOT and a reconciliation process to use with the Recruiting SSOT. The Recruiting SSOT will be used by Finance for forecasting and Recruiting for hiring. It will include all forecasted hiring for the fiscal year that is agreed upon by Finance and the GitLab [E-Group](/handbook/leadership/#e-group). 

## Exit Criteria

* Create a Recruiting SSOT document that has all the basic information needed for Recruiting to post reqs for the fiscal year and be able to contact hiring managers for additional detail
* Be able to reconcile GitLab's recruiting platform with the Recruiting SSOT on a monthly basis
* Be able to reconcile recent hires with Recruiting SSOT on a monthly basis
* Create processes for when changes are required from the Recruiting SSOT


## Roles and Responsibilities

| Working Group Role    | Person                | Title                                  |
|-----------------------|-----------------------|----------------------------------------|
| Facilitator           | Christine Machado     | Finance Business Partner               |
| Functional Lead       | Dave Gilbert          | VP, Recruiting                         |
| Member                | Erich Wegscheider     | Recruiting Program Analyst             |
| Member                | Chase Wright          | Manager, Financial Planning & Analysis |
| Member                | Kelly Murdock         | Manager, Recruiting                    |
| Member                | April Hoffbauer       | Manager, Recruiting Operations         |
| Member                | Kathleen Tam          | Manager, Data                          |
| Executive Stakeholder | Craig Mestel          | VP, Finance                            |
