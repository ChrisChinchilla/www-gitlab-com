---
layout: markdown_page
title: "Diversity and Inclusion Events"
---

## On this page
{:.no_toc}

- TOC
{:toc}

##  Introduction







Summary of Events for 2019:

| Month    | Events                                                       | Length                    |
|----------|--------------------------------------------------------------|---------------------------|
| Apr 2019 | Hired D&I Manager                                            |
| May 2019 | D&I Breakout Sessions at Contribute 2019                     | Yearly
| Jun 2019 | Monthly D&I Initiatives Call                                 | Monthly - Ongoing
|          | [Inclusive Language Training](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing)                                  | 
|          | Published GitLab D&I Mission Statement
|          | Published GitLab's Defintion of Diversity & Inclusion
| Jul 2019 | Launched Greenhouse Inclusion Tool
| Aug 2019 | Launched GitLab Pride                                                 | Ongoing
|          | Launched GitLab MIT - Minorities in Tech                              | Ongoing
|          | Launched GitLab DiversABILITY                                         | Ongoing
|          | Launched GitLab Women+                                                | Ongoing
|          | Launched D&I Advisory Group
| Sep 2019 | Published D&I Advisory Group Guidelines
|          | Published ERG Guidelines
|          | D&I Framework      
| Oct 2019 | Added Slack Channels for all ERGs and D&I Advisory Group
| Dec 2019 | Live Learning Inclusion Training
|          | Received D&I Comparably Award


Summary of Events for 2020:

| Month    | Events                                                        | Length                    |
|----------|---------------------------------------------------------------|---------------------------|
| Jan 2020 | Anita Borg contract signed                                    |                           |
|          | Live Learning Ally Training                                   | Short term                                       
|          | D&I Analytics Dashboard - First Iteration                     | Long term                 |
| Feb 2020 | Anita Borg becomes an Official Partner                        |
|          | D&I Survey via Culture Amp                                    | Short term                |
| Mar 2020 | Unconscious Bias Training                                     | Short term                |
|          | Working Mother Media Award Submission                         | Short term                |
|          | D&I Activities at Contribute                                  | Short term
| Apr      | Kickoff Women in Sales Initiatives                            | Long term
| Apr      | Kickoff D&I in Engineering Initiatives                        | Long term 
