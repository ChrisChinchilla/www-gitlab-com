---
layout: markdown_page
title: "Category Direction - Project Management"
---

- TOC
{:toc}

## Overview

Traditional project management is a set of tools and techniques which help organizations plan, manage, and deliver unique outcomes. Projects are typically one-off efforts, constrained by time and resources, where teams collaborate to invent and create new business capabilities.

The discipline of product management has emerged as a new paradigm in enterprise IT organizations, which blends the practices of ‘project management’ and ‘service management’ or ‘operations’.  Effectively product management is focused on optimizing the customer and business value of a specific product or service.  Product management combines the three concepts of product creation, product operation/improvement, and product shutdown under one umbrella.

GitLab ‘Project Management’ must adequately support the needs of both Traditional Project Management and the emerging demands of “Product Management” which continues to ascend in many organizations.

| Project Management  | Product Management  |
|---------------|----------------|
| Fixed time frame    |   Unspecified end   |
| Finite scope <br>(change management)   |   Evolving scope <br>(backlog management)  |
| Deliver on time/on budget  | Optimize for business Value |
| Budget constraints | Budget constraints |
| Resource constraints | Resource constraints |

<!-- ## Project management For the next iteration

* **Project Goals/Objectives.**  Projects are chartered in order to achieve a  specific outcome, which typically includes alignment to business objectives.
* **Budget and financial management.** Projects are typically allocated a finite budget to expand on execution.  The budget typically includes the cost of internal team members, external team members, and other expenses. Project budgets often include a mixture of both capital expenses and operating expenses and are expected to manage, track, and report on the financial status of the project. Typically time tracking is a key element included in financial management and reporting.
* **Project Schedule.**   Projects typically have an ultimate/anticipated completion date, against which progress is measured. Within the overall project schedule, there are frequently incremental milestones and deliverables.  Typically projects track dependencies between tasks using Gantt Charts to visualize both schedule and dependencies.
* **Scope management.** Because projects have limited budget and schedule, managing changes to scope help projects to adjust their commitments about cost and schedule constraints. The addition of new scope often coincides with additional budget or time to complete the project.   
* **Reporting and Dashboards.** Organizations need the ability to monitor and visualize how projects are progressing, as it relates to the larger plans and commitments. Notionally, this includes reports and dashboards that illustrate comparisons such as; schedule (plan vs actual), budget (plan vs actual), and scope (plan vs actual).  As projects roll up under larger business themes and objectives, reports will summarize how the organization is progressing against these strategic themes (aggregating the results from multiple projects). Many tasks are highlighted to execs via Red, Yellow, Green status updates and we should provide health/visibility on a dashboard so Project managers don’t have to create bespoke decks to show status.

## Product management
* **Product vision.** tbd
* **Business goals and objectives.** tbd
* **Budget and financial management.** tbd
* **Backlog management.** tbd
* **Reporting and dashboards.** tbd  -->

## GitLab projects

Project management in GitLab allows teams to plan, manage, and track work, usually software development work, to create potentially complex applications and services.

In particular, the primary object leveraged is the issue, which is the single source of truth for a given feature or any change in general, where team members collaborate on ideas and make decisions to move the process forward for that change. Attributes such as labels, milestones, assignees, and weights (story points) provide flexible ways for teams to do the otherwise difficult task of managing a large number of issues.

[Kanban boards](https://about.gitlab.com/direction/plan/kanban_boards/) are a powerful way to do that management as well, and it is its own product category. [Agile portfolio management](https://about.gitlab.com/direction/plan/agile_portfolio_management/) focuses on planning at a higher level work abstraction, and at longer time scales, and is also its own product category.

## From project to product

The future of project management is helping teams create and maintain long-lasting products that serve their customers. Traditional project management is very much focused on simply managing a project. A project is loosely defined as a set of requirements. And the purpose of project management here is to help teams ensure that the project is completed on time and on budget, based on the original agreed upon plan. (This concept will still be relevant for contracting scenarios. But GitLab is not focused on that use case.) The thinking used to be (and currently still is in many organizations), is that shipping product and features means completing a series of projects. This is an older mindset and GitLab is not focused on this way of helping teams, because we simply believe that there is a better way.

The project mindset suffers from being inward-focused, and puts too much emphasis on making sure plans are adhered to, for the sake of strategic planning and stability within an organization. The future that GitLab is participating in, and inventing, is _product management_. It is a focus on the customer and the business value delivered to them. It is outward-focused. If the process is focused on customers, then optimizations will be biased toward that instead. It gives room for Agile principles to breathe since the focus is not on strict constraints and change management. Nonetheless, we will still address the use cases of regulation, security, and compliance as needed in different industries. These will result in features that are targeted to meet those demands to mitigate _business risk_. But they will not be used to manage _project management risk_, which is the previous model. In summary, the project-to-product transition will allow customer needs and business concerns to _pull_ feature development. GitLab is focused on inventing this future.

Naming-wise, we don't anticipate that the _project management_ terminology to change because it is entrenched in the industry. This means we will have to continue educating our customers, and the industry at large and be intentional in explaining that GitLab project management is actually focused on developing products, and _not_ on managing projects.

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorites for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

Custom fields and custom workflows are a powerful notion to allow teams to support their very unique specific workflows, especially in large enterprises. Our first minimum viable change to address this need was [scoped labels](https://about.gitlab.com/blog/2019/06/20/issue-labels-can-now-be-scoped/); and we will continue on iterating on this concept to work towards a more native solution.

As well, a more powerful way to do backlog grooming and prioritization in general is also coming. This is somewhat possible right with issue boards, but we want to enable teams to manage even more issues' priorities altogether, even more easily.
See [Manual ordering of issue lists for backlog grooming prioritization](https://gitlab.com/groups/gitlab-org/-/epics/482).

Along with improved grooming capabilities, we are also focusing on [providing more insight into a team's velocity.](https://gitlab.com/gitlab-org/gitlab-ee/issues/13235).

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

The top competitor is Atlassian's Jira. It is by far the dominant and entrenched issue tracking system in the industry, and similar to GitLab, has expanded its offerings over the years. Interestingly to address Jira, we are not only offering competitive features, but also integration with
[Jira itself into GitLab merge requests](https://gitlab.com/gitlab-org/gitlab-ce/issues/27073), as a way to help customers to eventually make the transition to Jira, for example using an export/import migration strategy. See [CSV import](https://docs.gitlab.com/ee/user/project/issues/csv_import.html).

In recent years, there has been many smaller player to enter the project management space, to focus on more niche areas and solve more specific use cases. There are many. Some notable ones are Trello, Asana, Notion, Monday, and even Basecamp.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

Analysts in software project management, and in particular enterprise software management, are going beyond basic project management, as it applies to the software world. It is not sufficient to address this singular space and goal anymore. Analysts are looking at the bigger picture of DevOps, Agile transformation, and delivering business value to customers. In many ways, project management has become a subset of the greater goals of shipping value to customers as fast as possible, being able to get feedback as quickly as possible. So the relevant analyst domains are really enterprise agile planning and value stream management, which are two additional categories:
- [Agile Portfolio Management](../agile_portfolio_management/)
- [Value Stream Management](/direction/manage/value_stream_management/)

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

More and more customers are asking for manual prioritization beyond priority labels.
That's what [Manual ordering of issue lists for backlog grooming prioritization](https://gitlab.com/groups/gitlab-org/-/epics/482) addresses.
Additionally, folks need a way to do their own custom workflows with boards. That's what
[Group board with custom workflow](https://gitlab.com/groups/gitlab-org/-/epics/424) accomplishes.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Users in general have been asking for custom fields for a long time, and expect a mature project management tool to have such capability.
That's what [scoped labels](https://about.gitlab.com/blog/2019/06/20/issue-labels-can-now-be-scoped/).

Currently, Issue or MR description templates are scoped on a project level basis, but there is also a [desire to have them on a group level](https://gitlab.com/gitlab-org/gitlab-ee/issues/7749) so they can be shared across projects.

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

Teams are struggling with a way to manage priorities manually in GitLab. we have issue boards, but they are not sufficient. We need a way to manage a larger number of issues' priorities all at once, and that's what
[Manual ordering of issue lists for backlog grooming prioritization](https://gitlab.com/groups/gitlab-org/-/epics/482) accomplishes.

[Viewing a changelog of an Issue's description](https://gitlab.com/gitlab-org/gitlab-ee/issues/10103) has been a long requested feature. We're addressing this in 12.3.

Other notable items:

- [Support for Issue Dependencies](https://gitlab.com/gitlab-org/gitlab-ee/issues/2035)
- [Issue boards that can span across top level groups](https://gitlab.com/gitlab-org/gitlab-ce/issues/21497)

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

Unifying multiple projects under a standardized workflow is incredibly important for efficient agile planning at scale. [Managed workflows](https://gitlab.com/groups/gitlab-org/-/epics/424), adding better native support for [widely adopted agile methodologies](https://gitlab.com/gitlab-org/ux-research/issues/306), [exposing data to help teams make better planning decisions](https://gitlab.com/gitlab-org/gitlab-ee/issues/13235), and improving the exstensibility of GitLab via custom fields are top of mind. Underpinning this vision is the need to improve the ability to collaborate in a more [real time and reactive manner](https://gitlab.com/gitlab-org/gitlab-ce/issues/22675) within GitLab itself.
