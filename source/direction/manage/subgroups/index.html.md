---
layout: markdown_page
title: "Category Direction - Subgroups"
---

- TOC
{:toc}

| Category Attribute | Link | 
| ------ | ------ |
| [Stage](https://about.gitlab.com/handbook/product/categories/#hierarchy) | [Manage](https://about.gitlab.com/direction/manage) | 
| [Maturity](/direction/maturity/#maturity-plan) | [Not applicable](#maturity) |
| Labels | [groups](https://gitlab.com/groups/gitlab-org/-/epics?label_name=groups) |

## Introduction and how you can help
Thanks for visiting this category page on Subgroups within GitLab. This page belongs to the Spaces group of the Manage stage, and is maintained by [Luca Williams](https://gitlab.com/tipyn) who can be contacted directly via [email](mailto:luca@gitlab.com). You can also [open an issue](https://gitlab.com/gitlab-org/gitlab/issues) and @mention them there. Please add the `group::spaces` label for wider visibility.

This vision and direction is a work in progress and sharing your feedback directly on issues and epics on GitLab.com is the best way to contribute to this vision. If you’re a GitLab user and would like to talk to us about bringing the concept of Teams to GitLab and/or improving the overall user experience of Groups and Subgroups, we’d especially love to hear from you. 

## Groups

Groups are a fundamental building block (a [small primitive](https://about.gitlab.com/handbook/product/#prefer-small-primitives)) in GitLab for project organization and managing access to these resources at scale. Like folders in a filetree, they organize like resources into one place - whether that’s a handful of projects all belonging to a backend team, or organizing a group of user researchers into one place to make access control and communication easier.

Since groups cover multiple projects, we also scope a number of features at the group level like  epics, contribution analytics, and the security dashboard. 

In 2020, our goal is to improve groups with improvements to access control, enhancements to the user experience, and by introducing a first iteration at a **new type of group** specific for teams of people.

### Teams

Subgroups in GitLab are intentionally flexible. They're a great way of creating organization and access control, and do a good job of being a relatively generic object that's effective at both managing projects and organizing groups of people. In fact, our hypothesis is that these are the two big buckets that users tend to lean on groups for:

* Managing what gets worked on (the code). An organization might make a new group for a dedicated initiative and all its child projects. This specific namespace helps manage code that spans multiple projects.

* Managing the teams that do the work (the people). A group might be an organizational tool for a particular functional area (Sales, Marketing), and largely lean on issues to get things done.

### Why do we need Teams in GitLab? 

As GitLab's users have grown in scale, many instances have grown past the ability to use groups for people management. While Groups are a flexible small primitive we should retain, we're losing an opportunity to use this concept to help our customers get the most out of GitLab. People are not organised in the same way as code, and we should aim to accommodate our users now that GitLab has grown beyond the problems we initially solved with Groups. GitLab needs Teams for several reasons:

* **Planning** Planning at enterprise scale across hundreds of users and projects becomes difficult to manage. Since subgroups can be both groups of people and projects, they can't be used for capacity planning without explicitly defining a team as a group of people. With this capability, it will be possible to allocate the right people and the right _amount_ of people to work across projects, issues, MR's and epics.

* **Collaboration** With Teams, we would be able to explicitly map projects, groups, and other team members to a user in a single place. This gives us a way to communicate, collaborate, and push updates to a user without having to create issues or search for relevant issues or MRs.

## Target audience and experience

Groups are used by all GitLab users, no matter what role. However, heavier users of Subgroups in terms of organisation and management of projects and users consist of:

* **Group Owners**
* **System Admins**
* **Team Leaders, Directors, Managers**
* **Individual contributors** who predominantly work with GitLab's project management features

## Maturity

As Subgroups are a GitLab-specific concept, it's considered a non-marketing category without a [maturity level](/direction/maturity/) that can be compared to other competing solutions.

<!-- ## What's next & why -->

### Current

#### Better access control
Currently, adding new members to groups is largely permissive. Since groups tend to organize many important instance resources, we want to give administrators tools to restrict access to group resources and ensure that group members are in compliance.

We’re also improving on group authentication strategies that enterprises need to be successful (especially important for GitLab.com). Please see the ~authentication category epic for more detail.
* Related epics: https://gitlab.com/groups/gitlab-org/-/epics/84

### Next

#### Optional membership inheritance for subgroups
Currently, subgroups always inherit members from parent groups. Not being able to selectively remove subgroup membership from a top-level group makes groups challenging to organize, especially on GitLab.com - where most customers are represented by a single top-level group. These groups should have the ability to configure sub-groups with whatever visibility settings that are required.
* Related epics: https://gitlab.com/groups/gitlab-org/-/epics/588

<!-- ## Top user issue(s)

TBD

## Top internal customer issue(s)

TBD

## Top Vision Item(s)

TBD -->
