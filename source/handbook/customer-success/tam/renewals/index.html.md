---
layout: handbook-page-toc
title: "Customer Renewal Tracking"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

- [Technical Account Manager Summary](/handbook/customer-success/tam/)
- [CS Top 10](/handbook/customer-success/tam/cs-top-10/)
- [Capturing Customer Interest in GitLab Issues](/handbook/customer-success/tam/customer-issue-interest/)
- [Executive Business Reviews (EBRs)](/handbook/customer-success/tam/ebr/)
- [Account Engagement](/handbook/customer-success/tam/engagement/)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
- [Gemstones](/handbook/customer-success/tam/gemstones/)
- [Customer Health Scores](/handbook/customer-success/tam/health-scores/)
- [Account Onboarding](/handbook/customer-success/tam/onboarding/)
- [Customer Renewal Tracking](/handbook/customer-success/tam/renewals/) *(Current)*
- [Account Triage](/handbook/customer-success/tam/triage/)

### Related Pages

- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Responsibility Matrix and Transitions](/handbook/customer-success/#responsibility-matrix-and-transitions/)
- [Customer Success & Market Segmentation](/handbook/customer-success/#customer-success--market-segmentation/)
- [Customer Success Vision](/handbook/customer-success/vision/)
- [Support handbook](/handbook/support/)
- [Sales handbook](/handbook/sales/)

---

# Customer Renewal Tracking

A key part of the customer relationship lifecycle is the renewal phase.  TAMs must proactively track the renewal dates of their customers and align with their Strategic Account Manager (SAL) to ensure that a customer renewal takes place.

# Timeframe

At the beginning of each month a TAM should review their list of accounts in Salesforce and note the renewal dates for their accounts.  If a renewal date is within six months the TAM should schedule two “Renewal Review” meetings with the appropriate SAL and Solutions Architect (SA).  The first “Renewal Review” meeting should be scheduled five months out from the renewal date and the second should be scheduled three months out from the renewal date.

# Renewal Review Meeting

A “Renewal Review” meeting should have the following attendees:

 * Strategic Account Leader
 * Solutions Architect
 * Technical Account Manager

For strategic accounts (with a ACV over $250k) the following attendees should be added as well:

 * Regional Sales Director
 * TAM Regional Manager

The agenda of a “Renewal Review” meeting should include at least the following:

 1. Review of the customer health score over the last few months.
 1. Review of support issues and the underlying reasons for any escalations.
 1. Review of high priority feature requests.
 1. Review of the customer’s utilization of the product.
 1. Review of any changes in customer’s budget or staffing.

From this meeting a set of action items should be created to improve customer utilization and satisfaction with the product.  These items can include:

 1. **Architecture review** with Professional Services to address any underlying architectural weaknesses that could have contributed to an “Urgent” support escalation.
 1. **Product utilization review** to explore GitLab functionally that the customer is not using but could benefit from.
 1. **Roadmap review** to show the customer features that will be added to the product in the near term that may be valuable to them.  This could include a discussion with Product Management for strategic customers.

# Customer Cadence

The action items created from the “Renewal Review” meeting should be incorporated into the TAM customer cadence meetings and into any pending QBRs. The TAM should prioritize these reviews early in the renewal horizon.
