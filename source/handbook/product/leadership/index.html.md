---
layout: handbook-page-toc
title: Product Leadership
---

This document describes important leadership concepts specific to the product
management team. See also our page on [general GitLab leadership guidance](/handbook/leadership).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Product Leadership Team
The team members who report directly to the VP of Product are considered the [Product Leadership Team](https://gitlab.com/groups/gl-product-leadership/-/group_members).
This group can be referenced in GitLab.com issues using the `@gl-product-leadership` handle.

## Product Manager/Director Collaboration

As a product team leader, it's important to set the tone for the organization.
We put our PMs individually in the forefront as
[Directly Responsible Individuals (DRI)](/handbook/people-group/directly-responsible-individuals/),
with the trust and power that they can make the right decisions with our support as
product leaders. This section is intended to outline some best practices for
working between PMs and their directors. It contains guidance on responsibilities and
expectations the director should have in working with PMs, but is not intended
to be hard and fast rules that take the place of having a strong working relationship
and prioritizing things effectively together.

Note that this is intended as a supplement to the product director
[job description](/job-families/product/director-of-product/), with
specific focus on the interaction between PMs and their directors. General director
responsibilities can be found at that link.

An effective product director should:

- Respect that multiple voices influencing the group will be confusing to the
  execution team, and avoid coming out of left field with changing priorities.
  The PM owns the [day-to-day interaction](/handbook/product/#working-with-your-group)
  with their group and the director should influence through the directly responsible
  PM.

- Work through your PMs as your path to success, rather than focusing on individual
  achievement. We have a culture of empowering individual PMs, not centralizing
  authority, and need to continue to reinforce that as we grow.

- Help your PMs understand who [important internal and external customers](/handbook/product/#sensing-mechanisms)
  are for them, ensuring that there's a productive ongoing dialogue with feedback
  coming in and then features being adopted internally. [Internal customers](/handbook/values/#dogfooding)
  should have at least a monthly check-in, with the internal customer section
  in the category epic kept up to date.

- Represent their portfolio in day-to-day interaction with D and E groups, with
  particular responsibility to prioritize inputs from leadership into something
  stable in the short/medium term and actionable by the PMs, to align efforts
  across teams, and to raise visibility of wins by the PM to the broader team
  by being a "cheerleader" for the successes of their reports to the senior
  leadership group. This also includes publishing portfolio-level directional
  items that the rest of the company can use (and which should be reviewed and
  understood by the DRI PMs who will deliver this portfolio-level vision.)

- Support and hold PMs accountable for updates to the next release, 3 month vision, OKRs, etc.
  For teams using web category vision epics, this can be done in a very nice
  monthly update flow aligned to the releases and done via discussion in a comprehensive
  MR. Whatever the process, though, the director should be holding the PM accountable
  by providing support and guidance here, particularly when it comes to making sure
  organizational priorities are clear. Make sure upcoming portfolio-level milestones,
  due dates around releases, special projects with calendar impacts, etc. are visible
  and always clearly communicated.

- Actively coach your PMs, sensing where they can grow and do better. This
  coaching should be provided consistently, clearly, and in an actionable way, with
  the intent of avoiding "surprise problems". In the same vein, following up on
  requests for context, advice, and so on from the PMs is a priority. The
  [socratic method](/handbook/leadership/#management-group) is
  recommended as a great approach that works particularly well with PMs.

- Prioritize hiring, being sure to include PMs (and EMs/team members) who will work
  with the new person in the process.

- Provide structure and motivation for needed organizational changes (being more
  [data-driven](/handbook/product/#data-driven-work),
  telling stories, providing time for expansive thinking).

## How to be a Director of Product Management
This section describe important activities performed by Directors of Product Management.

### Product Walk Throughs
GitLab has [many product teams working on specific categories](). One way to ensure this growth in teams does not result in a disjointed experience for our users is to consistently perform Product Walk throughs that span categories and stages. Directors of Product schedule, at a minimum monthly, Product Walk throughs. To do so please create and follow the [Product Walk through issue template](https://gitlab.com/gitlab-com/Product/issues/new?issuable_template=Product-Walk-Through).
