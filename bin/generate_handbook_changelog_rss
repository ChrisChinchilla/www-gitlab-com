#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'
require 'gitlab'
require_relative '../lib/changelog'

REL_FILE_PATH = "source/handbook/changelog.rss"
FILE_PATH = File.expand_path(REL_FILE_PATH)

Changelog::RSS.create(FILE_PATH)

CM_ID = 3623028
PROJECT_ID = Changelog::WWW_GITLAB_COM_PROJECT_ID
TARGET_BRANCH = 'master'
GENERATED_TIMESTAMP = DateTime.now.to_date.to_s
SOURCE_BRANCH = "changelog-rss-#{GENERATED_TIMESTAMP}"

Gitlab.create_branch(PROJECT_ID, SOURCE_BRANCH, TARGET_BRANCH)
Gitlab.create_commit(PROJECT_ID, SOURCE_BRANCH, "Update changelog RSS feed for #{GENERATED_TIMESTAMP}",
                     [action: 'update', file_path: REL_FILE_PATH, content: File.read(FILE_PATH)])

mr = Gitlab.create_merge_request(PROJECT_ID, "Update changelog RSS feed for #{GENERATED_TIMESTAMP}",
                            source_branch: SOURCE_BRANCH, target_branch: TARGET_BRANCH,
                            assignee_id: CM_ID, remove_source_branch: true,
                            labels: 'no changelog')

# TODO: move this into a shared module for generate_handbook_changelog to use also, once tested
# and remove logging
retry_count = 0
puts "Initial MR merge_status: #{mr.merge_status}"
while %w[checking unchecked].include?(mr.merge_status) && retry_count < 10
  sleep(1)
  mr = Gitlab.merge_request(PROJECT_ID, mr.iid)
  retry_count += 1
  puts "After #{retry_count} retries, status is #{mr.merge_status}"
end
result = Gitlab.accept_merge_request(PROJECT_ID, mr.iid, merge_when_pipeline_succeeds: true)
puts "Accepted MR:"
puts result.inspect
